trigger StockCount_Trigger on Stock_Count__c (after insert, after update, after delete) {
    if(trigger.isAfter) {
        if(trigger.isInsert) {
            StockCount_TriggerHandler.setProductStockCountFlag(trigger.new);
        }
        if(trigger.isDelete) {
            StockCount_TriggerHandler.unSetProductStockCountFlag(trigger.old);
        }          
    }
}
trigger OpportunityLineItem_Trigger on OpportunityLineItem (after insert, after update, after delete) {
    if(trigger.isAfter) {
        if(trigger.isInsert) {
            OpportunityLineItem_TriggerHandler.addLineItems(trigger.new);
        }
        if(trigger.isUpdate) {
            OpportunityLineItem_TriggerHandler.changeLineItems(trigger.old,trigger.new);
        }
        if(trigger.isDelete) {
            OpportunityLineItem_TriggerHandler.deleteLineItems(trigger.old);
        }                
    }
}

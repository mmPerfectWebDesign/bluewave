public with sharing class OpportunityLineItem_TriggerHandler {

    public static Boolean accessible = Schema.SObjectType.Stock_Count__c.fields.Stock_Count__c.isAccessible();

    public static void addLineItems (List<OpportunityLineItem> lineItemTriggerRecords) {

        // get a Set of products
        Set<ID> productIDSet = new Set<ID>();

        for (OpportunityLineItem oli : lineItemTriggerRecords) {
            productIDSet.add(oli.Product2Id);
        }

        // get a list of stock count records where product in products set
        List<Stock_Count__c> StockCountList = [SELECT Product__c,Stock_Count__c FROM Stock_Count__c WHERE Product__c IN :productIDSet];

        // create a map of product id to related stock count record
        Map<ID, Stock_Count__c> productStockCountMap = new Map<ID, Stock_Count__c>();
        for(Stock_Count__c stkCnt : StockCountList) {
            productStockCountMap.put(stkCnt.Product__c,stkCnt);
        }        

        // loop over trigger, get product id and quantity,
        for (OpportunityLineItem oli : lineItemTriggerRecords) {
            if(!accessible) { oli.addError('User Profile Does Not Have Access');}

            Decimal oliQty = oli.Quantity;
            // access map for stock count by product
            Stock_Count__c oliStkCnt = productStockCountMap.get(oli.Product2Id);

            // update stock count quantity 
            oliStkCnt.Stock_Count__c = oliStkCnt.Stock_Count__c - oliQty;

            // Check this doesn't create a negative stock value and return error if it does.
            if(oliStkCnt.Stock_Count__c < 0) {
                oli.addError('Cannot Have Negative Stock.');
            }

            // put stock count back in map
            productStockCountMap.put(oli.Product2Id,oliStkCnt);
        }

        // create new stock count list for update
        List<Stock_Count__c> StockCountListToUpdate = new List<Stock_Count__c>();

        // iterate over map add stock count to list
        for(Stock_Count__c StockCount : productStockCountMap.values()){
            StockCountListToUpdate.add(StockCount);
        }

        //update list  
        if(!StockCountListToUpdate.isEmpty()){
            Update StockCountListToUpdate;
        }  
    }

    public static void changeLineItems (List<OpportunityLineItem> OldRecords,List<OpportunityLineItem> lineItemTriggerRecords) {

        // get a Set of products
        Set<ID> productIDSet = new Set<ID>();

        for (OpportunityLineItem oli : lineItemTriggerRecords) {
            productIDSet.add(oli.Product2Id);
        }

        // get a list of stock count records where product in products set
        List<Stock_Count__c> StockCountList = [SELECT Product__c,Stock_Count__c FROM Stock_Count__c WHERE Product__c IN :productIDSet];

        // create a map of product id to related stock count record
        Map<ID, Stock_Count__c> productStockCountMap = new Map<ID, Stock_Count__c>();
        for(Stock_Count__c stkCnt : StockCountList) {
            productStockCountMap.put(stkCnt.Product__c,stkCnt);
        }

        // loop over trigger, get product id and quantity,
        for (OpportunityLineItem oli : lineItemTriggerRecords) {
            if(!accessible) { oli.addError('User Profile Does Not Have Access');}

            Decimal oliQty = oli.Quantity;
            // access map for stock count by product
            Stock_Count__c oliStkCnt = productStockCountMap.get(oli.Product2Id);

            // Loop over old records to get previous stock quantity
            Decimal oldOliQty;
            for (OpportunityLineItem oldOli : OldRecords) {
                if(oldOli.id == oli.id) {
                    oldOliQty = oldOli.Quantity; 
                    break;
                }
            }
            // update stock count quantity 
            oliStkCnt.Stock_Count__c = oliStkCnt.Stock_Count__c - oliQty + oldOliQty;

            // Check this doesn't create a negative stock value and return error if it does.
            if(oliStkCnt.Stock_Count__c < 0) {
                oli.addError('Cannot Have Negative Stock.');
            }            

            // put stock count back in map
            productStockCountMap.put(oli.Product2Id,oliStkCnt);
        }

        // create new stock count list for update
        List<Stock_Count__c> StockCountListToUpdate = new List<Stock_Count__c>();

        // iterate over map add stock count to list
        for(Stock_Count__c StockCount : productStockCountMap.values()){
            StockCountListToUpdate.add(StockCount);
        }

        //update list  
        if(!StockCountListToUpdate.isEmpty()){
            Update StockCountListToUpdate;
        }  
    }
    
    public static void deleteLineItems (List<OpportunityLineItem> lineItemTriggerRecords) {
        // get a Set of products
        Set<ID> productIDSet = new Set<ID>();

        for (OpportunityLineItem oli : lineItemTriggerRecords) {
            if(!accessible) { oli.addError('User Profile Does Not Have Access');}
            productIDSet.add(oli.Product2Id);
        }

        // get a list of stock count records where product in products set
        List<Stock_Count__c> StockCountList = [SELECT Product__c,Stock_Count__c FROM Stock_Count__c WHERE Product__c IN :productIDSet];

        // create a map of product id to related stock count record
        Map<ID, Stock_Count__c> productStockCountMap = new Map<ID, Stock_Count__c>();
        for(Stock_Count__c stkCnt : StockCountList) {
            productStockCountMap.put(stkCnt.Product__c,stkCnt);
        }

        // loop over trigger, get product id and quantity,
        for (OpportunityLineItem oli : lineItemTriggerRecords) {
            Decimal oliQty = oli.Quantity;
            // access map for stock count by product
            Stock_Count__c oliStkCnt = productStockCountMap.get(oli.Product2Id);

            // update stock count quantity 
            oliStkCnt.Stock_Count__c = oliStkCnt.Stock_Count__c + oliQty;

            // put stock count back in map
            productStockCountMap.put(oli.Product2Id,oliStkCnt);
        }

        // create new stock count list for update
        List<Stock_Count__c> StockCountListToUpdate = new List<Stock_Count__c>();

        // iterate over map add stock count to list
        for(Stock_Count__c StockCount : productStockCountMap.values()){
            StockCountListToUpdate.add(StockCount);
        }

        //update list  
        if(!StockCountListToUpdate.isEmpty()){
            Update StockCountListToUpdate;
        }  
    }  
}

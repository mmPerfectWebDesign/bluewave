@isTest(seeAllData=false)
public with sharing class OpportunityLineItem_TriggerHandler_Test {

    @TestSetup
    static void createSeedData(){

        ID priceBookID = Test.getStandardPricebookId();

        Opportunity Opp = new Opportunity();
        Opp.Name = 'The Opp';
        Opp.CloseDate = system.today();
        Opp.StageName = 'Qualification';
        Opp.Pricebook2Id = priceBookID;
        insert Opp;

        Product2 Prod = new Product2();
        Prod.Name = 'The Product';
        insert Prod;

        PricebookEntry pbe = new PricebookEntry(pricebook2id=priceBookID, product2id=Prod.id,unitprice=2.0, isActive=true);
        insert pbe;        

        Stock_Count__c SC = new Stock_Count__c();
        SC.Product__c = Prod.ID;
        SC.Stock_Count__c = 10;
        insert SC;

        Product2 ProdNoSC = new Product2();
        ProdNoSC.Name = 'A Product without a Stock Count';
        insert ProdNoSC;

        PricebookEntry pbeNoSC = new PricebookEntry(pricebook2id=priceBookID, product2id=ProdNoSC.id,unitprice=2.0, isActive=true);
        insert pbeNoSC;         

    }

    @isTest
    static void insert_record_succcess() {
        Opportunity theOpportunity =    [SELECT Name,Pricebook2Id from Opportunity LIMIT 1];
        Product2 theProduct =           [SELECT Name from Product2 WHERE Name = 'The Product' LIMIT 1];
        PricebookEntry pbe =            [SELECT id from PricebookEntry WHERE pricebook2id=:theOpportunity.Pricebook2Id AND product2id=:theProduct.id];

        Test.startTest();

        // Insert a new line item with a quantity of 3
        OpportunityLineItem line = new OpportunityLineItem();
        line.OpportunityId = theOpportunity.ID;
        line.Product2Id = theProduct.ID;
        line.PricebookEntryId = pbe.ID;
        line.Quantity = 3;
        line.unitPrice = 5;
        insert line;

        //Get the stock count record and check that the 3 has been subtracted from the original 10
        Stock_Count__c newStockCount = [SELECT ID, Stock_Count__c,Product__c FROM Stock_Count__c WHERE Product__c = :theProduct.ID];

        Decimal correctStockCount = 10 - line.Quantity;
        System.assertEquals(correctStockCount,newStockCount.Stock_Count__c);

        Test.stopTest();
    }

    @isTest
    static void update_exisiting_record_success() {
        Opportunity theOpportunity =    [SELECT Name,Pricebook2Id from Opportunity LIMIT 1];
        Product2 theProduct =           [SELECT Name from Product2 WHERE Name = 'The Product' LIMIT 1];
        PricebookEntry pbe =            [SELECT id from PricebookEntry WHERE pricebook2id=:theOpportunity.Pricebook2Id AND product2id=:theProduct.id];

        // Insert a new line item with a quantity of 3
        OpportunityLineItem line = new OpportunityLineItem();
        line.OpportunityId = theOpportunity.ID;
        line.Product2Id = theProduct.ID;
        line.PricebookEntryId = pbe.ID;
        line.Quantity = 3;
        line.unitPrice = 5;
        insert line;

        Test.startTest();

        // Update line item with a quantity of 4
        line.Quantity = 4;
        update line;

        //Get the stock count record and check that 4 has now been subtracted from the original 10
        Stock_Count__c newStockCount = [SELECT ID, Stock_Count__c,Product__c FROM Stock_Count__c WHERE Product__c = :theProduct.ID];

        Decimal correctStockCount = 10 - line.Quantity;
        System.assertEquals(correctStockCount,newStockCount.Stock_Count__c);

        Test.stopTest();        
    }

    @isTest
    static void delete_exisiting_record_success() {
        Opportunity theOpportunity =    [SELECT Name,Pricebook2Id from Opportunity LIMIT 1];
        Product2 theProduct =           [SELECT Name from Product2 WHERE Name = 'The Product' LIMIT 1];
        PricebookEntry pbe =            [SELECT id from PricebookEntry WHERE pricebook2id=:theOpportunity.Pricebook2Id AND product2id=:theProduct.id];

        // Insert a new line item with a quantity of 3
        OpportunityLineItem line = new OpportunityLineItem();
        line.OpportunityId = theOpportunity.ID;
        line.Product2Id = theProduct.ID;
        line.PricebookEntryId = pbe.ID;
        line.Quantity = 3;
        line.unitPrice = 5;
        insert line;

        Test.startTest();

        delete line;

        //Get the stock count record and check that the 3 has NOT been subtracted from the original 10
        Stock_Count__c newStockCount = [SELECT ID, Stock_Count__c,Product__c FROM Stock_Count__c WHERE Product__c = :theProduct.ID];

        Decimal correctStockCount = 10;
        System.assertEquals(correctStockCount,newStockCount.Stock_Count__c);

        Test.stopTest();            
    }


    @isTest
    static void insert_creating_negative_stock() {
        Opportunity theOpportunity =    [SELECT Name,Pricebook2Id from Opportunity LIMIT 1];
        Product2 theProduct =           [SELECT Name from Product2 WHERE Name = 'The Product' LIMIT 1];
        PricebookEntry pbe =            [SELECT id from PricebookEntry WHERE pricebook2id=:theOpportunity.Pricebook2Id AND product2id=:theProduct.id];

        Test.startTest();

        try {
            // Insert a new line item with a quantity of 11 to attempt a negative stock count
            OpportunityLineItem line = new OpportunityLineItem();
            line.OpportunityId = theOpportunity.ID;
            line.Product2Id = theProduct.ID;
            line.PricebookEntryId = pbe.ID;
            line.Quantity = 11;
            line.unitPrice = 5;
            insert line;
        } catch (Exception e) {
            Boolean expectedExceptionThrown =  (e.getMessage().contains('Cannot Have Negative Stock.')) ? true : false; 
                System.AssertEquals(true, expectedExceptionThrown, e.getMessage()); 
        }

        Test.stopTest();
    }   
    

    @isTest
    static void update_creating_negative_stock() {
        Opportunity theOpportunity =    [SELECT Name,Pricebook2Id from Opportunity LIMIT 1];
        Product2 theProduct =           [SELECT Name from Product2 WHERE Name = 'The Product' LIMIT 1];
        PricebookEntry pbe =            [SELECT id from PricebookEntry WHERE pricebook2id=:theOpportunity.Pricebook2Id AND product2id=:theProduct.id];

        // Insert a new line item with a quantity of 3
        OpportunityLineItem line = new OpportunityLineItem();
        line.OpportunityId = theOpportunity.ID;
        line.Product2Id = theProduct.ID;
        line.PricebookEntryId = pbe.ID;
        line.Quantity = 3;
        line.unitPrice = 5;
        insert line;

        Test.startTest();

        try {
            line.Quantity = 11;
            update line;

        } catch (Exception e) {
            Boolean expectedExceptionThrown =  (e.getMessage().contains('Cannot Have Negative Stock.')) ? true : false; 
             System.AssertEquals(true, expectedExceptionThrown, e.getMessage()); 
        }

        Test.stopTest();   
    }

    
    @isTest
    static void insert_against_missing_record() {
        Opportunity theOpportunity =    [SELECT Name,Pricebook2Id from Opportunity LIMIT 1];
        Product2 theProduct =           [SELECT Name from Product2 WHERE Name = 'A Product without a Stock Count' LIMIT 1];
        PricebookEntry pbe =            [SELECT id from PricebookEntry WHERE pricebook2id=:theOpportunity.Pricebook2Id AND product2id=:theProduct.id];

        Test.startTest();

        try {
            // Insert a new line item with a quantity of 11 to attempt a negative stock count
            OpportunityLineItem line = new OpportunityLineItem();
            line.OpportunityId = theOpportunity.ID;
            line.Product2Id = theProduct.ID;
            line.PricebookEntryId = pbe.ID;
            line.Quantity = 11;
            line.unitPrice = 5;
            insert line;
        } catch (Exception e) {
            Boolean expectedExceptionThrown =  (e.getMessage().contains('Product Must Have Stock Count Record')) ? true : false; 
                System.AssertEquals(true, expectedExceptionThrown, e.getMessage()); 
        }

        Test.stopTest();
    }
  
    
    @isTest
    static void bulk_insert_records() {
        Opportunity theOpportunity =    [SELECT Name,Pricebook2Id from Opportunity LIMIT 1];
        Product2 theProduct =           [SELECT Name from Product2 WHERE Name = 'The Product' LIMIT 1];
        PricebookEntry pbe =            [SELECT id from PricebookEntry WHERE pricebook2id=:theOpportunity.Pricebook2Id AND product2id=:theProduct.id];

        Test.startTest();

        // Insert a 3 line items with a total quantity of 6
        List<OpportunityLineItem> lines = new List<OpportunityLineItem>();

        OpportunityLineItem line1 = new OpportunityLineItem();
        line1.OpportunityId = theOpportunity.ID;
        line1.Product2Id = theProduct.ID;
        line1.PricebookEntryId = pbe.ID;
        line1.Quantity = 3;
        line1.unitPrice = 5;
        lines.add(line1);

        OpportunityLineItem line2 = new OpportunityLineItem();
        line2.OpportunityId = theOpportunity.ID;
        line2.Product2Id = theProduct.ID;
        line2.PricebookEntryId = pbe.ID;
        line2.Quantity = 2;
        line2.unitPrice = 5;
        lines.add(line2);            
        
        OpportunityLineItem line3 = new OpportunityLineItem();
        line3.OpportunityId = theOpportunity.ID;
        line3.Product2Id = theProduct.ID;
        line3.PricebookEntryId = pbe.ID;
        line3.Quantity = 1;
        line3.unitPrice = 5;  
        lines.add(line3);                      

        insert lines;

        //Get the stock count record and check that the 3 has been subtracted from the original 10
        Stock_Count__c newStockCount = [SELECT ID, Stock_Count__c,Product__c FROM Stock_Count__c WHERE Product__c = :theProduct.ID];

        Decimal correctStockCount = 10 - line1.Quantity - line2.Quantity - line3.Quantity;
        System.assertEquals(correctStockCount,newStockCount.Stock_Count__c);

        Test.stopTest();        
    }


    @isTest
    static void insert_with_Invalid_Profile_record() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Bluewave User']; 
        User u = new User(Alias = 'ro1',
                            Email='standarduser@testorg.com', 
                            EmailEncodingKey='UTF-8', LastName='Testing',
                            LanguageLocaleKey='en_US', 
                            LocaleSidKey='en_US',
                            ProfileId = p.Id, 
                            TimeZoneSidKey='America/Los_Angeles',
                            UserName='standarduser@testorgmeb.com');

        Opportunity theOpportunity =    [SELECT Name,Pricebook2Id from Opportunity LIMIT 1];
        Product2 theProduct =           [SELECT Name from Product2 WHERE Name = 'The Product' LIMIT 1];
        PricebookEntry pbe =            [SELECT id from PricebookEntry WHERE pricebook2id=:theOpportunity.Pricebook2Id AND product2id=:theProduct.id];

        System.runAs(u) {
            Test.startTest();
            try {
                // Insert a new line item with a quantity of 3
                OpportunityLineItem line = new OpportunityLineItem();
                line.OpportunityId = theOpportunity.ID;
                line.Product2Id = theProduct.ID;
                line.PricebookEntryId = pbe.ID;
                line.Quantity = 3;
                line.unitPrice = 5;
                correctStockCount = 10 - line.Quantity;
                insert line;
            } catch (Exception e) {
                Boolean expectedExceptionThrown =  (e.getMessage().contains('User Profile Does Not Have Access')) ? true : false; 
                System.AssertEquals(true, expectedExceptionThrown, e.getMessage()); 
            }
            Test.stopTest();
        }
    }
}
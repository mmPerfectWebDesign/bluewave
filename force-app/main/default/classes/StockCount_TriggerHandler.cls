public without sharing class StockCount_TriggerHandler {
    public static void setProductStockCountFlag(List<Stock_Count__c> StockCountRecords ) {
        Set<ID> productIDSet = new Set<ID>();
        for(Stock_Count__c sc : StockCountRecords) {
            productIDSet.add(sc.Product__c);
        }

        List<Product2> products = [SELECT Id,Has_Stock_Count__c FROM Product2 WHERE ID IN : productIDSet ];

        for(Product2 theProduct : products) {
            theProduct.Has_Stock_Count__c = true;
        }

        update products;
    }
    public static void unSetProductStockCountFlag(List<Stock_Count__c> StockCountRecords ) {
        Set<ID> productIDSet = new Set<ID>();
        for(Stock_Count__c sc : StockCountRecords) {
            productIDSet.add(sc.Product__c);
        }

        List<Product2> products = [SELECT Id,Has_Stock_Count__c FROM Product2 WHERE ID IN : productIDSet ];

        for(Product2 theProduct : products) {
            theProduct.Has_Stock_Count__c = false;
        }

        update products;
    }    
}

@isTest(seeAllData=false)
    public with sharing class StockCount_TriggerHandler_Test {
    
        @TestSetup
        static void createSeedData(){
            Product2 ProductWithoutSCRecord = new Product2();
            ProductWithoutSCRecord.Name = 'Product 1';
            ProductWithoutSCRecord.Has_Stock_Count__c = false;
            insert ProductWithoutSCRecord;    
        }

        @isTest
        static void insert_exisiting_record() {
            // This test would test the validation rule failure on Stock_Count__c using ProductWithSCRecord            
        }

        @isTest
        static void testProductUpdatedOnAddAndDelete() {

            Product2 productToTest = [SELECT Id,Has_Stock_Count__c FROM Product2 WHERE Name = 'Product 1' LIMIT 1];
            // It starts as false
            System.assertEquals(false,productToTest.Has_Stock_Count__c);

            Stock_Count__c stockCount = new Stock_Count__c();
            stockCount.Product__c = productToTest.ID;
            stockCount.Stock_Count__c = 10;
            insert stockCount;

            Product2 productWithSC = [SELECT Id,Has_Stock_Count__c FROM Product2 WHERE Name = 'Product 1' LIMIT 1];
            // It now been set to true
            System.assertEquals(true,productWithSC.Has_Stock_Count__c); 
            
            Stock_Count__c stockCountToDelete = [SELECT Id,Product__c FROM Stock_Count__c WHERE Product__c = :productWithSC.Id ];
            delete stockCountToDelete;

            Product2 productWithDeletedSC = [SELECT Id,Has_Stock_Count__c FROM Product2 WHERE Name = 'Product 1' LIMIT 1];
            // The flag should now be back to false
            System.assertEquals(false,productWithDeletedSC.Has_Stock_Count__c);             
        }
    }